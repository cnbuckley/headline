package com.headline.copy.models;

import lombok.Builder;
import lombok.Getter;

import java.util.Date;

@Getter
public class CopyHistory extends PersistentClass {

    private final Copy copy;
    private final User user;
    private final Date modifiedOn;
    private final String action;

    @Builder
    public CopyHistory(Long id, Copy copy, User user, Date modifiedOn, String action) {
        super(id);
        this.copy = copy;
        this.user = user;
        this.modifiedOn = modifiedOn;
        this.action = action;
    }
}
