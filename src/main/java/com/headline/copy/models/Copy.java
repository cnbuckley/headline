package com.headline.copy.models;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;

/**
 * This is the Copy, it uses a template to govern what goes into the content.
 *
 * For example:
 * Say you wanted to get the copy that uses the default template, in US English, and has a key1=value
 *
 * API call (search):
 * https://api.headline.com/copy?template=default&languageCode=en-US&key1=value
 *
 *  {
 *      "template" : 1L,
 *      "organization" : 1L,
 *      "version" : 1,
 *      "languageCode" : "en-US"
 *      "content" : { //comes from the template.contentMetaDescription
 *          "key" : "value",
 *          "key1" : "value"
 *      }
 *  }
 *
 *  We expose all the common REST calls.
 *  ie. https://api.headline.com/copy/<some_id>
 */
@Getter
public class Copy extends PersistentClass{

    private final CopyTemplate template;
    private final Organization organization;
    private final Integer version;
    private final String languageCode;
    private final Map<String, String> content;

    @Builder
    public Copy(Long id, CopyTemplate template, Organization organization, Integer version, String languageCode, Map<String, String> content) {
        super(id);
        this.template = template;
        this.organization = organization;
        this.version = version;
        this.languageCode = languageCode;
        this.content = content;
    }
}
