package com.headline.copy.models;

import lombok.Builder;
import lombok.Getter;

@Getter
public class Organization extends PersistentClass{

    private final String name;

    @Builder
    public Organization(String id, String name) {
        super(id);
        this.name = name;
    }
}
