package com.headline.copy.models;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * Meta data that describes copy.  The idea is you can create templates for copy and request the copy for that template.
 *
 * For example:
 * Say you wanted to have a template for mobile, ie. your copy writers will write for smaller screens. You would
 * create a "template" called "mobile" and then create copy for that template.
 *
 * API call:
 * https://api.headline.com/template/default
 *
 * Response:
 *  {
 *      "name" : "default",
 *      "contentMetaDescription": {
 *          "key" : {
 *              "required" : true,
 *              "maxLength" : 100,
 *              "validator" : "custom_validator"
 *          },
 *          "key1" : {
 *              "required" : false,
 *              "maxLength" : 100,
 *              "validator" : "custom_validator"
 *          }
 *      },
 *      "organization" : 1L,
 *      "version" : 1,
 *      "actions" : ["custom_publish_on", "foo", "bar"], // used to trigger custom events like sending messages
 *      "state" : "DRAFT"
 *  }
 */
@Getter
public class CopyTemplate extends PersistentClass {

    private final String name;
    private final Map<String, Map<String, String>> contentMetaDescription;
    private final Organization organization;
    private final Integer version;
    private final List<String> actions;
    private final String state;

    @Builder
    public CopyTemplate(Long id, String name, Map<String, Map<String, String>> contentMetaDescription, Organization organization, Integer version, List<String> actions, String state) {
        super(id);
        this.name = name;
        this.contentMetaDescription = contentMetaDescription;
        this.organization = organization;
        this.version = version;
        this.actions = actions;
        this.state = state;
    }
}
