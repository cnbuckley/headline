package com.headline.copy.models;

import lombok.Builder;
import lombok.Getter;

import java.util.List;


@Getter
public class User extends PersistentClass{

    private final String name;
    private final String iconPath;
    private final String email;
    private final String phone;
    private final List<Organization> organizations;

    @Builder
    public User(Long id, String name, String iconPath, String email, String phone, List<Organization> organizations) {
        super(id);
        this.name = name;
        this.iconPath = iconPath;
        this.email = email;
        this.phone = phone;
        this.organizations = organizations;
    }
}
