package com.headline.copy.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class PersistentClass implements Serializable {

    private final Long id;
}
